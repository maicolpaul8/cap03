import './App.css';
import styled from 'styled-components';

import Card from './Card';
import Submenu from './Submenu';
import { useState } from 'react';

const Container=styled.div`

`;
const App=()=>{

  const [data,setData]=useState([
    {
      id:1,
      color:"#21d0d0",
      options:[
        {
            title:"PRICE LOW TO HIGH",
            state:false
        },
        {
            title:"PRICE HIGH TO LOW",
            state:false
        },
        {
            title:"popularity",
            state:true
        }                         
    ],
    footer:1
    },
    {
      id:2,
      color:"#ff7745",
      options:[
        {
            title:"1up nutrition",
            state:false
        },
        {
            title:"asitis",
            state:false
        },
        {
            title:"avvatar",
            state:false
        },
        {
          title:"big muscles",
          state:false
        },
        {
          title:"bpi sports",
          state:false
        },
        {
          title:"bsn",
          state:false
        },   
        {
          title:"cellucor",
          state:false
        },  
        {
          title:"domin8r",
          state:false
        },
        {
          title:"dymatize",
          state:true
        }    
    ],
    footer:2
    }
  ]);

  const handleChange=(value,id)=>{
  
       const match =data.filter(el=>el.id===id)[0];
       
       const act =match.options.filter(el=>el.title===value)[0];

       const copy=[...match.options];

       const targetIndex=copy.findIndex(f=>f.title===value);

       if(targetIndex>-1){
         copy[targetIndex]={title:value,state:!act.state};
         //setData(copy);
         match.options=copy;
       }

       const copyList=[...data];

       const targetIndexList=copyList.findIndex(f=>f.title===id);

       if(targetIndexList>-1){
        copy[targetIndexList]=match;        
      }
      setData(copyList);
  }

  const handleReset=(id)=>{


    const copy=[...data];

    const targetIndexList=copy.findIndex(f=>f.id===id);

    if(targetIndexList>-1){

      var raw=[];

        const options=data.filter(el=>el.id===id)[0].options;

        for (let x of options) raw.push({title:x.title,status:false})

        copy[targetIndexList].options=raw;        
    }
   setData(copy);


  }

  return (<Container>
    <Card></Card>
    {
      data.map((v,i)=><Submenu {...v} onChange={handleChange} onReset={handleReset}></Submenu>)    
    }
    
  </Container>);
}

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

export default App;
