import React from "react";

import  styled  from "styled-components";

import Options from './options';

import Footer from './footer';

const Container=styled.div`
    margin-top:15px;
    //background-color:#21d0d0;
    background-color:${props=>props.color};
    border-radius:25px;
    padding:15px;   
    //height:180px;
    width:400px;    
    padding-top:15px; 
`;

const App=(params)=>{

    const handleChange=(value)=>{
        if (typeof params.onChange==='function') params.onChange(value,params.id);
        //console.log(value);

    };

    const handleReset=()=>{
        if (typeof params.onReset==='function') params.onReset(params.id);
        //console.log(value);

    };
   

    return (
        <Container color={params.color}>
            {
                params.options.map((v,i)=>
                   <Options key={i} {...v} onChange={handleChange}>

                    </Options>
                )
            }  
            <Footer version={params.footer} onReset={handleReset} >             
                
            </Footer>          
        </Container>
    );

}

export default App;