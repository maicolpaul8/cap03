import React  from "react";

import  styled  from "styled-components";

import check from '../images/1398913_circle_correct_mark_success_tick_icon.png';

const Container=styled.div`
    margin-left:25px;
    margin-top:15px;
    display:flex;
    flex:direction:row;
    justify-content:space-between;
    margin-right:25px;
`
const Title=styled.p`
    margin:0px;
    color:white;
    font-weight:500;

`
export const Circulo=styled.div`
    border-radius:50%;
    border:1.8px solid white;
    height:35px;
    width:35px;
    cursor:pointer;
    background-color:${props=>props.visible ? 'white' : 'transparent'};
    `;

export const Icon=styled.img`
        height:35px;
        width:35px;
        object-fit:contain;
        visibility:${props=>props.visible ? 'visible' : 'hidden'};
    `

const App=({title,state,onChange})=>{

    // const [visible,setVisible]=useState(false);

    // useEffect(()=>{
    //     // if(state){
    //     //     setVisible(true);
    //     // }else{
    //     //     setVisible(false);
    //     // }
    //     setVisible(state);
    // },[state]);

    const handleChange=()=>{
        //setVisible(!state);       
        if (typeof onChange==='function')  onChange(title);
    }

    return (
        <Container>
            <Title>{title}</Title>
            <Circulo visible={state} onClick={handleChange}>
                <Icon src={check} visible={state}></Icon>
            </Circulo>            
        </Container>
    );

}

export default App;