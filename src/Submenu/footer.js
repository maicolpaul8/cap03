import React from "react";

import  styled  from "styled-components";

import {Circulo,Icon} from './options';

import check from '../images/1398913_circle_correct_mark_success_tick_icon.png';

import cancel from '../images/1398919_close_cross_incorrect_invalid_x_icon.png';

import reset from '../images/172536_reload_icon.png';

const Container=styled.div`
display:flex;
justify-content:center;
align-items:center;
flex-direction:row;
`;

const ContainerV1=styled.div`
display:flex;
justify-content:center;
align-items:center;
flex-direction:row;
margin-top:15px;
padding-top:18px;
padding-bottom:15px;
border-top:1px solid #ffffff80;
`;

const Item=({v,onClick})=>{
    return(<Circulo visible={true} style={{marginLeft:15,height:60,width:60}} onClick={onClick}>                    
        <Icon src={v} visible={true} style={{height:60,width:60}}></Icon>
    </Circulo> )        
}


const App=({version,onReset,onCancel,onAcept})=>{

    const handleReset=()=>{
            console.log("vamos a Resetear");

            if (typeof onReset==='function') onReset()
    }
    const handleOK=()=>{
        console.log("vamos a OK");
        if (typeof onReset==='function') onAcept()
}
    const handleCancel=()=>{
        console.log("vamos a Cancelar");
        if (typeof onReset==='function') onCancel()
    }

    if(version===1)
    {
        let data=[
            {icon:cancel,onClick:handleCancel},
            {icon:check,onClick:handleOK}            
        ]
        return (
            <Container>  
                    {
                        data.map((v,i)=><Item v={v.icon} onClick={v.onClick}></Item>                           
                        )
                    }                   
            </Container>
        );
    }else{
        let data=[
            {icon:cancel,onClick:handleCancel},            
            {icon:reset,onClick:handleReset},
            {icon:check,onClick:handleOK},
        ]
        return (
            <ContainerV1>  
                    {
                        data.map((v,i)=><Item v={v.icon} onClick={v.onClick}></Item>                           
                        )
                    }                   
            </ContainerV1>
        );
    }
}

export default App;