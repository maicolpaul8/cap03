import React from "react";

import styled  from "styled-components";

const Container=styled.div`
    height:150px;
    width:350px;
    border-radius:5px;
    background-color:#fff0ea;

    display:flex;
    justify-conten:center;
    align-items:center;
    flex-direction:column;
`;

const Title=styled.p`
    color:#f06f37;
    text-transform:uppercase;
    font-size:15px;
    font-weight:bold;    
    width:300px;
    text-align:center;    
`

const Button=styled.button`
    background-color:#f06f37;
    text-transform:uppercase;
    border:1px solid #f06f37;
    padding:7px;
    padding-left:25px;
    padding-right:25px;
    color:white;
    font-weight:bold;
    border-radius:15px;   
    opacity:0.9;
    cursor:pointer; 
`

const App=()=>{
    return (<Container>
            <Title>
                use code :pigi199 for.100 off on you fist order
            </Title>
            <Button>
                grab now
            </Button>
    </Container>);
  }

  export default App;
